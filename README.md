# MemoCache

Cache in memory using a priority queue

[![version](https://img.shields.io/npm/v/memocache?color=blue&label=version)](https://www.npmjs.com/package/memocache)
[![downloads](https://img.shields.io/npm/dm/memocache?color=blue)](https://gitlab.com/rodrigobuas/memocache/-/commits/develop)
[![collaborators](https://img.shields.io/npm/collaborators/memocache?label=collaborators&color=blue)](https://gitlab.com/rodrigobuas/memocache/-/commits/develop)
[![lines](https://img.shields.io/tokei/lines/gitlab/rodrigobuas/memocache?label=lines)](https://gitlab.com/rodrigobuas/memocache/-/commits/develop)

Data flux ios library

## 🚨 Build Health

[![pipeline](https://gitlab.com/rodrigobuas/memocache/badges/develop/pipeline.svg)](https://gitlab.com/rodrigobuas/memocache/-/commits/develop)
[![coverage](https://gitlab.com/rodrigobuas/memocache/badges/develop/coverage.svg)](https://gitlab.com/rodrigobuas/memocache/-/commits/develop)
[![quality](https://sonarcloud.io/api/project_badges/measure?project=memocache&branch=develop&metric=alert_status)](https://sonarcloud.io/dashboard?id=memocache&branch=develop)

[![reliability](https://sonarcloud.io/api/project_badges/measure?project=memocache&branch=develop&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=memocache&branch=develop)
[![security](https://sonarcloud.io/api/project_badges/measure?project=memocache&branch=develop&metric=security_rating)](https://sonarcloud.io/dashboard?id=memocache&branch=develop)
[![maintainability](https://sonarcloud.io/api/project_badges/measure?project=memocache&branch=develop&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=memocache&branch=develop)
[![vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=memocache&branch=develop&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=memocache&branch=develop)
[![bugs](https://sonarcloud.io/api/project_badges/measure?project=memocache&metric=bugs)](https://sonarcloud.io/dashboard?id=memocache&branch=develop)
[![duplications](https://sonarcloud.io/api/project_badges/measure?project=memocache&branch=develop&metric=duplicated_lines_density)](https://sonarcloud.io/dashboard?id=memocache&branch=develop)
[![smells](https://sonarcloud.io/api/project_badges/measure?project=memocache&branch=develop&metric=code_smells)](https://sonarcloud.io/dashboard?id=memocache&branch=develop)
[![debt](https://sonarcloud.io/api/project_badges/measure?project=memocache&branch=develop&metric=sqale_index)](https://sonarcloud.io/dashboard?id=memocache&branch=develop)

- [vulnerabilities audit](https://rodrigobuas.gitlab.io/memocache/vulnerabilities.html)
- [licenses summary](https://rodrigobuas.gitlab.io/memocache/licenses-summary.txt)
- [unit test coverage report](https://rodrigobuas.gitlab.io/memocache/coverage-unit/lcov-report/index.html)
- [integration test coverage report](https://rodrigobuas.gitlab.io/memocache/coverage-int/lcov-report/index.html)
- [integration tests report](https://rodrigobuas.gitlab.io/memocache/int-test/index.html)

## ✨ Features

- MemoryCache : a class to keep a cache in memory using PriorityQueue
- PriorityQueue : a class to keep a list of priorities

## 🎧 Dev Features

- Typescript libraty compilation
- Unit tests using jest
- Code quality repports
- ESLint for static code analyse
- Prettier for code style consistency
- Editor config settings for file format consistency
- Versioned yarn as package manager
- Husky in order to control push process and commit rules
- Commit linter with standards versions following conventions of [conventional commits](https://www.conventionalcommits.org/)
- Change log automatic generation
- Version bump management based on commit categories

## ⚙️ Setup

- run the command `make setup` to setup automatically your local environment with [development tools](/docs/setup) or follow the [step by step](/docs/setup#install-development-tools).

## 🎁 How to use it

Run `make dev` in order to develop and start docker images in dev mode. Or run `make` to see all available commands.

Otherwise, if you prefer, you can run scripts directly from the project code. Run `yarn run` to see all scripts configurated on `package.json`, exemple : `yarn start:watch` to start a stand alone instance of the server in watch mode.

## 💎 How to test it

Run `make test` in order to run all pipeline tests or run specifically a test listed on project :

- `make tl` : static test on src code
- `make tr` : vulnerability test on package and its dependencies
- `make tu` : unit test on src code

## 🚀 CI / CD

This project follows the gitflow Workflow to implement [continuous integration](/docs/cicd.md#continuous-integration) and [continuous deployement](/docs/cicd.md#continuous-deployment).

## 📖 Documentations

TODO: add documentations about how to documentation works on it

## 🏆 Quality

In order to monitoring the quality, this project is equiped with sonar configuration.<br>
You can find the sonar results in [sonar](https://sonarcloud.io/dashboard?id=memocache).

## ♻️ Contributing

Do you want to evolve this project ? Your are more than welcome :-)<br>
Take a look in our [code of conduct to contribute](/CONTRIBUTING.md) and the process for submitting pull requests to us.
