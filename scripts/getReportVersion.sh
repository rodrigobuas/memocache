#################################################
#### Usage: getReportVersion.sh <version>
#### EX: getReportVersion.sh 1.20.3
#################################################

export RELEASE_VERSION=$1

if [ "${CI_COMMIT_REF_NAME}" == "develop" ]; then REPORT_VERSION="v${RELEASE_VERSION}-wip"; else REPORT_VERSION="v${RELEASE_VERSION}"; fi

echo ${REPORT_VERSION}
