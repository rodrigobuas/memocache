import differenceInMilliseconds from 'date-fns/differenceInMilliseconds';

import { PriorityQueue } from '../PriorityQueue';

export interface MemoryCacheOptions {
  ttl?: number; // time to live in milleseconds
  max?: number;
}

export class MemoryCache<T> {
  public memory: Record<string, T>;
  private memoryAt: Record<string, number>;
  private priority: PriorityQueue;
  private readonly cacheTTL: number;

  constructor({ ttl = 0, max }: MemoryCacheOptions = {}) {
    this.cacheTTL = ttl;
    this.priority = new PriorityQueue(max);
    this.reset();
  }

  reset(): void {
    this.memory = {};
    this.memoryAt = {};
    this.priority.reset();
  }

  get(key: string): T {
    if (this.memory[key] === undefined) {
      return undefined;
    }
    if (this.isExpired(key)) {
      this.unset(key);
      return undefined;
    }

    this.priority.push(key);
    return this.memory[key];
  }

  set(key: string, record: T): T {
    this.memory[key] = record;
    this.memoryAt[key] = Date.now();
    this.priority.push(key);
    this.purge();
    return record;
  }

  unset(key: string): T {
    const record = this.memory[key];
    delete this.memoryAt[key];
    delete this.memory[key];
    this.priority.pull(key);
    return record;
  }

  purge(): void {
    // purge expired
    if (this.cacheTTL) {
      this.priority.forEach((key: string) => this.isExpired(key) && this.unset(key));
    }

    // purge excess
    if (this.priority.max) {
      this.priority.excess.forEach((key: string) => this.unset(key));
    }
  }

  isExpired(key: string): boolean {
    const { cacheTTL } = this;
    if (!cacheTTL) {
      return false;
    }

    const record = this.memory[key];
    const cacheTime = this.memoryAt[key];
    if (!record || !cacheTime) {
      return true;
    }

    const now = Date.now();
    const age = differenceInMilliseconds(now, cacheTime);
    return age > cacheTTL;
  }
}
