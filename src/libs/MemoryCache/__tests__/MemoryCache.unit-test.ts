import { describe, it, expect } from '@jest/globals';

import { MemoryCache } from '../MemoryCache';

function sleep(time): Promise<void> {
  return new Promise((resolve) => setTimeout(resolve, time));
}

describe('MemoryCache', () => {
  it('should get undefined when cache key is not set yet', () => {
    // given
    const cache = new MemoryCache();
    // when
    const record = cache.get('a');
    // then
    expect(record).toBeUndefined();
  });

  it('should set key on cache', () => {
    // given
    const cache = new MemoryCache();
    // when
    const record = cache.set('a', 'AAA');
    const stored = cache.get('a');
    // then
    expect(record).toBe('AAA');
    expect(stored).toBe('AAA');
  });

  it('should not get after unset', () => {
    // given
    const cache = new MemoryCache();
    // when
    const record = cache.set('a', 'AAA');
    const removed = cache.unset('a');
    const stored = cache.get('a');
    // then
    expect(record).toBe('AAA');
    expect(removed).toBe('AAA');
    expect(stored).toBeUndefined();
  });

  it('should not get after reset', () => {
    // given
    const cache = new MemoryCache();
    // when
    const record = cache.set('a', 'AAA');
    cache.reset();
    const stored = cache.get('a');
    // then
    expect(record).toBe('AAA');
    expect(stored).toBeUndefined();
  });

  it('should purge excess', () => {
    // given
    const cache = new MemoryCache({ max: 2 });
    // when
    cache.set('a', 'AAA');
    cache.set('b', 'BBB');
    cache.set('c', 'CCC');
    cache.set('d', 'DDD');
    // then
    expect(cache.memory).toEqual({
      c: 'CCC',
      d: 'DDD',
    });
  });

  it('should purge expired', async () => {
    // given
    const cache = new MemoryCache({ ttl: 100 });
    cache.set('a', 'AAA');
    cache.set('b', 'BBB');
    cache.set('c', 'CCC');
    await sleep(200);
    cache.set('d', 'DDD');
    // when
    const aRecord = cache.get('a');
    const dRecord = cache.get('d');
    // then
    expect(aRecord).toBeUndefined();
    expect(dRecord).toBe('DDD');
  });

  it('should record not be expired', async () => {
    // given
    const cache = new MemoryCache({ ttl: 100 });
    cache.set('a', 'AAA');
    // when
    const isExpired = cache.isExpired('a');
    // then
    expect(isExpired).toBe(false);
  });

  it('should record be expired', async () => {
    // given
    const cache = new MemoryCache({ ttl: 100 });
    cache.set('a', 'AAA');
    await sleep(200);
    // when
    const isExpired = cache.isExpired('a');
    // then
    expect(isExpired).toBe(true);
  });

  it('should record be expired', async () => {
    // given
    const cache = new MemoryCache({ ttl: 100 });
    await sleep(200);
    // when
    const isExpired = cache.isExpired('a');
    // then
    expect(isExpired).toBe(true);
  });

  it('should record be expired', async () => {
    // given
    const cache = new MemoryCache({ ttl: 100 });
    cache.set('a', 'AAA');
    await sleep(200);
    // when
    const record = cache.get('a');
    // then
    expect(record).toBeUndefined();
  });
});
