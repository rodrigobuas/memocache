import { describe, it, expect } from '@jest/globals';

import { PriorityQueue } from '../PriorityQueue';

describe('PriorityQueue', () => {
  it('must instantiate with an empty array', () => {
    // when
    const queue = new PriorityQueue();
    // then
    expect(queue.max).toBe(0);
    expect(queue.length).toBe(0);
    expect(queue.fifo).toEqual([]);
  });

  it('must instantiate with max 3', () => {
    // when
    const queue = new PriorityQueue(3);
    // then
    expect(queue.max).toBe(3);
    expect(queue.length).toBe(0);
  });

  it('must instantiate with max 3', () => {
    // when
    const queue = new PriorityQueue(3);
    // then
    expect(queue.max).toBe(3);
    expect(queue.length).toBe(0);
  });

  it('must add item on queque', () => {
    // given
    const queue = new PriorityQueue();
    // when
    queue.push('a');
    // then
    expect(queue.length).toBe(1);
    expect(queue.fifo).toEqual(['a']);
  });

  it('must add multiple items on queque', () => {
    // given
    const queue = new PriorityQueue();
    // when
    queue.push('a');
    queue.push('b');
    queue.push('c');
    // then
    expect(queue.length).toBe(3);
    expect(queue.fifo).toEqual(['c', 'b', 'a']);
  });

  it('must add twice the same key on queque', () => {
    // given
    const queue = new PriorityQueue();
    // when
    queue.push('a');
    queue.push('b');
    queue.push('c');
    queue.push('a');
    // then
    expect(queue.length).toBe(3);
    expect(queue.fifo).toEqual(['a', 'c', 'b']);
  });

  it('must reset the queque', () => {
    // given
    const queue = new PriorityQueue();
    queue.push('a');
    queue.push('b');
    queue.push('c');
    // when
    queue.reset();
    // then
    expect(queue.length).toBe(0);
    expect(queue.fifo).toEqual([]);
  });

  it('must pull item from a queque', () => {
    // given
    const queue = new PriorityQueue();
    queue.push('a');
    queue.push('b');
    queue.push('c');
    // when
    queue.pull('c');
    // then
    expect(queue.length).toBe(2);
    expect(queue.fifo).toEqual(['b', 'a']);
  });

  it('must not change queue when pull a non existent item', () => {
    // given
    const queue = new PriorityQueue();
    queue.push('a');
    queue.push('b');
    queue.push('c');
    // when
    queue.pull('d');
    // then
    expect(queue.length).toBe(3);
    expect(queue.fifo).toEqual(['c', 'b', 'a']);
  });

  it('must pop an existent item', () => {
    // given
    const queue = new PriorityQueue();
    queue.push('a');
    queue.push('b');
    queue.push('c');
    // when
    queue.pop();
    // then
    expect(queue.length).toBe(2);
    expect(queue.fifo).toEqual(['c', 'b']);
  });

  it('must pop multiple item', () => {
    // given
    const queue = new PriorityQueue();
    queue.push('a');
    queue.push('b');
    queue.push('c');
    queue.push('a');
    // when
    queue.pop();
    queue.pop();
    // then
    expect(queue.length).toBe(1);
    expect(queue.fifo).toEqual(['a']);
  });

  it('must traverse a c b', () => {
    // given
    const queue = new PriorityQueue();
    queue.push('a');
    queue.push('b');
    queue.push('c');
    queue.push('a');
    const expected = ['a', 'c', 'b'];
    // when
    queue.forEach((value: string, index: number) => {
      // then
      expect(value).toBe(expected[index]);
    });
  });

  it('must dont have an excess', () => {
    // given
    const queue = new PriorityQueue();
    queue.push('a');
    queue.push('b');
    queue.push('c');
    // then
    expect(queue.excess).toEqual([]);
  });

  it('must have one excess', () => {
    // given
    const queue = new PriorityQueue(2);
    queue.push('a');
    queue.push('b');
    queue.push('c');
    // then
    expect(queue.excess).toEqual(['a']);
  });
});
