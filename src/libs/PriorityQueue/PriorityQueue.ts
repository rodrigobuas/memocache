import pull from 'lodash/pull';

export class PriorityQueue {
  public fifo: string[];
  public readonly max: number;

  constructor(max = 0) {
    this.max = max;
    this.reset();
  }

  get length(): number {
    return this.fifo.length;
  }

  push(key: string): void {
    this.pull(key);
    this.fifo.unshift(key);
  }

  pop(): string {
    return this.fifo.pop();
  }

  reset(): void {
    this.fifo = [];
  }

  pull(key: string): void {
    pull(this.fifo, key);
  }

  forEach(callback: (value: string, index: number, array: string[]) => void): void {
    this.fifo.forEach(callback);
  }

  get excess(): string[] {
    if (this.max <= 0) {
      return [];
    }
    return this.fifo.slice(this.max);
  }
}
