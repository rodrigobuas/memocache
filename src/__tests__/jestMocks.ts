import isNil from 'lodash/isNil';

import { HEADER_TOTAL_COUNT, RestResponse } from '../libs/RestBridge/RestBridge.types';

export function mockResponse<T>(statusCode = 200, body: T = null, totalCount?: number): RestResponse<T> {
  const headers = !isNil(totalCount) ? { [HEADER_TOTAL_COUNT]: totalCount.toString() } : undefined;
  return { body, statusCode, headers };
}
