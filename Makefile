.EXPORT_ALL_VARIABLES:

CI_COMMIT_REF_NAME ?= $(shell git branch --show-current)
CI_COMMIT_REF_SLUG ?= $(shell git branch --show-current | sed -e 's/\//-/g')
CI_COMMIT_SHORT_SHA ?= $(shell git rev-parse --short HEAD)

BUILD_VERSION=${CI_COMMIT_REF_SLUG}/${CI_COMMIT_SHORT_SHA}
RELEASE_VERSION=$$(./scripts/readVersionFromPackage.sh)
VERSION_TARGET ?= patch

help:  ## print this list
	@egrep -h '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'


test:  ## run all tests pipeline in local
	make tl
	make tr
	make tu


tl:  ## run code linter
	yarn test:lint


tr:  ## run reports tests
	yarn reports


tu:  ## run unit tests
	yarn test:unit:report


tag_release:  ## merge and tag the current release/hotfix branch onto origin/master
	@echo "Bumping version ..."
	git checkout develop
	yarn release --skip.commit --release-as ${VERSION_TARGET}
	git add package.json
	git add CHANGELOG.md
	git commit -am "🚀: release ${RELEASE_VERSION} from ${CI_COMMIT_REF_NAME} (${CI_COMMIT_SHORT_SHA})"
	@echo "Adding tag v${RELEASE_VERSION} ..."
	git tag v${RELEASE_VERSION}
	git push origin --tags
	@echo "Pushing version into develop ..."
	git push -u origin develop


npm_publish:  ## publish new version into npm
	@echo "Publishing tagged release into package manager ..."
	@echo "//registry.npmjs.org/:_authToken=${NPM_TOKEN}" > dist/.npmrc
	cd dist && npm publish
	rm -rf .npmrc
