---
path: /setup
title: Setup
order: 3
---

# Setup environment

Run the command `make setup` to setup automatically your local environment or follow the step by step bellow.

## Install development tools

- install [yarn (package manager)](https://yarnpkg.com/)

Or you can let YVM (Yarn Version Manager) manage it for you doing :

- `curl -fsSL https://raw.githubusercontent.com/tophat/yvm/master/scripts/install.sh | bash`
- `yvm install`
- `yvm use`

Or you can install direct a yarn version using the command :

```bash
curl -o- -L https://yarnpkg.com/install.sh | bash -s -- --version [version]
```

- install [NVM (Node Version Manager)](https://github.com/creationix/nvm)

```bash
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.2/install.sh | bash
```

- Install [AVN (Automatic Version Switching for Node.js)](https://github.com/wbyoung/avn)

```bash
npm install -g avn avn-nvm avn-n
avn setup
```

- Install NodeJs [NodeJs](https://nodejs.org/en/)

```bash
nvm install
```

- Test installation

```bash
node -v
npm -v
yarn -v
```
