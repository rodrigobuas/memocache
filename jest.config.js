module.exports = {
  verbose: true,
  clearMocks: true,
  testEnvironment: 'node',
  transform: {
    '^.+\\.tsx?$': 'ts-jest',
  },
  preset: 'ts-jest',
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
  testRegex: '/__tests__/.*test\\.(ts|js)$',
  coverageDirectory: 'reports/coverage-unit',
  collectCoverageFrom: [
    'src/**/*.{js,ts}',
    '!src/**/*.d.ts',
    '!src/*.ts',
    '!src/**/index.js',
    '!src/**/index.ts',
    '!<rootDir>/src/tests.ts',
    '!<rootDir>/node_modules/',
    '!<rootDir>/**/__tests__/**',
    '!<rootDir>/**/__steps__/**',
  ],
  coverageThreshold: {
    global: {
      branches: 70,
      functions: 60,
      lines: 70,
      statements: 70,
    },
  },
  testPathIgnorePatterns: ['dist'],
};
